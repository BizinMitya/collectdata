package com.collectdata.model;

import java.util.List;

public class Data {
    private String OS;
    private int SDK;
    private List<String> apps;

    public Data() {
    }

    public Data(String OS, int SDK, List<String> apps) {
        this.OS = OS;
        this.SDK = SDK;
        this.apps = apps;
    }


    public String getOS() {
        return OS;
    }

    public void setOS(String OS) {
        this.OS = OS;
    }

    public int getSDK() {
        return SDK;
    }

    public void setSDK(int SDK) {
        this.SDK = SDK;
    }

    public List<String> getApps() {
        return apps;
    }

    public void setApps(List<String> apps) {
        this.apps = apps;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Версия ОС: ").append(getOS()).append("\n")
                .append("Версия SDK: ").append(getSDK()).append("\n")
                .append("Список установленных приложений: ").append("\n");
        for (String app : apps) {
            stringBuffer.append(app).append("\n");
        }
        return String.valueOf(stringBuffer);
    }
}
